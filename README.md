# README #

This repository is meant for providing an updated version of my CV to whoever is interested. The CV is available in English and German. Any comments or questions are welcome. Feel free to contact me @ syed <dot> ali <dot> abbas <dot> jaffari <at> gmail <dot> com.

### How to get the CV? ###

# Download directly via direct static link
* [English Version](https://bitbucket.org/syed_ali_abbas_jaffari/syed_cv/raw/master/syed_cv_en.pdf)
* [German Version](https://bitbucket.org/syed_ali_abbas_jaffari/syed_cv/raw/master/syed_cv_de.pdf)

# Download using the browser
* Click on the 'Source' option from the menu on the left hand side.
* Click the pdf with the desired language ('en' for English, 'de' for German).
* Click on 'View Raw' option to download the pdf.

# Download using Git
* Type the following in a Linux terminal:
'''git clone git@bitbucket.org:syed_ali_abbas_jaffari/syed_cv.git'''
* Move to the newly cloned folder 'syed_cv'. Both the CVs will be there.
